import rpy2.robjects as robjects
from decimal import *
from tools import *

class Year:
	def __init__(self, y, r):
		self.year = y
		self.records = {}

		home_goals, road_goals = [0, 0], [0, 0]
		home_saves, road_saves = [0, 0], [0, 0]

		for i in range(0, len(r), 4):
			hg, rg = parse(r[i]), parse(r[i + 1])
			hs, rs = parse(r[i + 2]), parse(r[i + 3])

			home_goals = [a + b for a, b in zip(hg, home_goals)]
			road_goals = [a + b for a, b in zip(rg, road_goals)]
			home_saves = [a + b for a, b in zip(hs, home_saves)]
			road_saves = [a + b for a, b in zip(rs, road_saves)]

		self.records['Goals'] = [home_goals, road_goals]
		self.records['Saves'] = [home_saves, road_saves]

		self.results = {}

	def get_proportions(self):
		p = ""

		for k in sorted(self.results.keys()):
			p += k + " " + str(self.results[k]) + "\n"

		return p

	def __repr__(self):
		return self.name + "\n" + str(self.records)

years = []

for line in open("raw2b.csv", 'r'):
    tmp = line.rstrip('\n\r ').split(',')

    years.append(Year(tmp[0], tmp[1:]))

for y in years:
	for k in y.records.keys():
		r = y.records[k]

		y.results[k] = z_test(robjects.IntVector((r[0][0], r[1][0])), robjects.IntVector((r[0][1], r[1][1])), "greater")

for y in years:
	print(y.year + "\n" + y.get_proportions())

# Pooling everything together

hg, hgt, rg, rgt, hs, hst, rs, rst = 0, 0, 0, 0, 0, 0, 0, 0

for y in years:
	g, s = y.records['Goals'], y.records['Saves']

	hg += g[0][0]
	hgt += g[0][1]
	rg += g[1][0]
	rgt += g[1][1]
	hs += s[0][0]
	hst += s[0][1]
	rs += s[1][0]
	rst += s[1][1]

print("Pooled")
print("Goals " + z_test(robjects.IntVector((hg, rg)), robjects.IntVector((hgt, rgt)), "greater"))
print("Saves " + z_test(robjects.IntVector((hs, rs)), robjects.IntVector((hst, rst)), "greater"))