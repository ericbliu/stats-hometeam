import rpy2.robjects as robjects
from decimal import *
from tools import *

class League:
	def __init__(self, n, r):
		self.name = n
		self.records = {}

		home_total, road_total = [0, 0], [0, 0]

		for i in range(0, len(r), 7):
			home, road = [0, 0], [0, 0]

			for j in range(i + 1, i + 7, 2):
				home = [a + b for a, b in zip(home, parse(r[j]))]
				road = [a + b for a, b in zip(road, parse(r[j + 1]))]

			self.records[str(r[i])] = [home, road]

			home_total = [a + b for a, b in zip(home, home_total)]
			road_total = [a + b for a, b in zip(road, road_total)]

		self.records['Tot.'] = [home_total, road_total]

		self.results = {}

	def get_proportions(self):
		p = ""

		for k in sorted(self.results.keys()):
			p += k + " " + str(self.results[k]) + "\n"

		return p

	def __repr__(self):
		return self.name + "\n" + str(self.records)

leagues = []

for line in open("raw1.csv", 'r'):
    tmp = line.rstrip('\n\r ').split(',')

    leagues.append(League(tmp[0], tmp[1:]))

for l in leagues:
	for k in l.records.keys():
		r = l.records[k]

		l.results[k] = z_test(robjects.IntVector((r[0][0], r[1][0])), robjects.IntVector((r[0][1], r[1][1])), "greater")

for l in leagues:
	print(l.name + "\n" + l.get_proportions())

# Pooling everything together

hw, ht, aw, at = 0, 0, 0, 0

for l in leagues:
	r = l.records['Tot.']

	hw += r[0][0]
	ht += r[0][1]
	aw += r[1][0]
	at += r[1][1]

print("Pooled\n" + z_test(robjects.IntVector((hw, aw)), robjects.IntVector((ht, at)), "greater"))