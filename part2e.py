import rpy2.robjects as robjects
from decimal import *
from tools import *

class Year:
    def __init__(self, y, r):
        self.year = y

        self.home, self.road = [], []

        for i in range(0, len(r), 2):
            self.home.append(int(r[i]))
            self.road.append(int(r[i + 1]))

        self.results = ""

    def get_proportions(self):
        return self.year + " " + str(self.results)

    def __repr__(self):
        return self.year + " " + str(self.home) + " " + str(self.road)

years = []

for line in open("raw2e.csv", 'r'):
    tmp = line.rstrip('\n\r ').split(',')

    years.append(Year(tmp[0], tmp[1:]))

for y in years:
    y.results = t_test(robjects.IntVector(y.home), robjects.IntVector(y.road), "greater", p = True, ve = True)

for y in years:
        print(y.get_proportions())

# Pooling everything together

home, road = [], []

for y in years:
    home += y.home
    road += y.road

print("Pooled " + t_test(robjects.IntVector(home), robjects.IntVector(road), "greater", p = True, ve = True))