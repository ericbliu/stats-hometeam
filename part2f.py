import rpy2.robjects as robjects
from decimal import *
from tools import *

class Year:
    def __init__(self, y, r):
        self.year = y
        self.records = []

        home_wins, road_wins = Decimal("0"), Decimal("0")
        games = Decimal("324")

        for i in range(0, len(r), 2):
            home_wins += Decimal(r[i]) * games
            road_wins += Decimal(r[i + 1]) * games

        self.records.append([round(home_wins), int(games * 3)])
        self.records.append([round(road_wins), int(games * 3)])

        self.results = ""

    def get_proportions(self):
        return self.year + " " + str(self.results)

    def __repr__(self):
        return self.year + " " + str(self.records)

getcontext().rounding = ROUND_HALF_UP

years = []

for line in open("raw2f.csv", 'r'):
    tmp = line.rstrip('\n\r ').split(',')

    years.append(Year(tmp[0], tmp[1:]))

for y in years:
    r = y.records
    y.results = z_test(robjects.IntVector((r[0][0], r[1][0])), robjects.IntVector((r[0][1], r[1][1])), "greater")

for y in years:
    print(y.get_proportions())

# Pooling everything together

hw, ht, rw, rt = 0, 0, 0, 0

for y in years:
    r = y.records

    hw += r[0][0]
    ht += r[0][1]
    rw += r[1][0]
    rt += r[1][1]

print("Pooled " + z_test(robjects.IntVector((hw, rw)), robjects.IntVector((ht, rt)), "greater"))