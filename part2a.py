import rpy2.robjects as robjects
from decimal import *
from tools import *

class Year:
	def __init__(self, y, r):
		self.year = y
		self.records = []

		home_wins, road_wins = [0, 0], [0, 0]

		for i in range(0, len(r), 2):
			hw, rw = parse(r[i]), parse(r[i + 1])

			home_wins = [a + b for a, b in zip(hw, home_wins)]
			road_wins = [a + b for a, b in zip(rw, road_wins)]

		self.records.append(home_wins)
		self.records.append(road_wins)

		self.results = 0

	def get_proportions(self):
		return self.year + " " + str(self.results)

	def __repr__(self):
		return self.name + "\n" + str(self.records)

years = []

for line in open("raw2a.csv", 'r'):
    tmp = line.rstrip('\n\r ').split(',')

    years.append(Year(tmp[0], tmp[1:]))

for y in years:
	r = y.records
	y.results = z_test(robjects.IntVector((r[0][0], r[1][0])), robjects.IntVector((r[0][1], r[1][1])), "greater")

for y in years:
	print(y.get_proportions())

# Pooling everything together

hw, ht, rw, rt = 0, 0, 0, 0

for y in years:
	r = y.records

	hw += r[0][0]
	ht += r[0][1]
	rw += r[1][0]
	rt += r[1][1]

print("Pooled " + z_test(robjects.IntVector((hw, rw)), robjects.IntVector((ht, rt)), "greater"))