1 Atlanta Hawks
2 Boston Celtics
3 Charlotte Bobcats
4 Chicago Bulls
5 Cleveland Cavaliers
6 Dallas Mavericks
7 Denver Nuggets
8 Detroit Pistons
9 Golden State Warriors
10 Houston Rockets
11 Indiana Pacers
12 Los Angeles Clippers
13 Los Angeles Lakers
14 Memphis Grizzlies
15 Miami Heat
16 Milwaukee Bucks
17 Minnesota Timberwolves
18 New Jersey Nets
19 New Orleans Hornets
20 New York Knicks
21 Oklahoma City Thunder/Seattle Supersonics
22 Orlando Magic
23 Philadelphia 76ers
24 Phoenix Suns
25 Portland Trail Blazers
26 Sacramento Kings
27 San Antonio Spurs
28 Toronto Raptors
29 Utah Jazz
30 Washington Wizards

2011-2012 (22 5 1)
ORLANDO 503/750 492/758
CLEVELAND 561/790 617/855
ATLANTA 578/760 447/625

2010-2011 (24 19 5)
PHOENIX 764/1023 708/916 
NEW ORLEANS 700/940 752/957
CLEVELAND 764/1037 782/1038

2009-2010 (11 29 14)
INDIANA 783/1012 781/1007
UTAH 849/1160 805/1073
MEMPHIS 804/1086 810/1116

2008-2009 (23 7 29)
PHILADELPHIA 832/1102 820/1114
DENVER 1023/1363 868/1124
UTAH 911/1175 903/1177

2007-2008 (24 21 19)
PHOENIX 838/1072 710/905
OKLAHOMA CITY 742/948 694/918
NEW ORLEANS 693/902 619/805