% !TEX root = Writeup.tex

\section{Results}

\singlespace
\noindent\begin{tabular}{|p{\textwidth}|}\hline
\small This section contains a summary of the results of applying statistical procedures to our data. The raw data is listed in Appendix {\sc a}. Intermediate calculations, graphs, and plots are found in Appendix {\sc b}.\\\hline
\end{tabular}\onehalfspace\\

\noindent In Part 1, we considered home and road win proportions. If there is no home team advantage, we would expect the proportions to be roughly equal; the effect of various other factors, such as injuries, would be expected to balance out over the course of a season. The data was tested using a one-tail two-proportion $z$ procedure at the $5\%$ level. In the results below, $p_1$ refers to the proportion of home games won and $p_2$ to the proportion of away games won.\\

\noindent\begin{tabular}{crp{12cm}}
\multicolumn{3}{l}{\textsf{\textbf{Part 1: Win Proportions}}}\\
\multicolumn{3}{l}{\small \textsf{Raw data on page \pageref{raw_part1}; calculations on page \pageref{part1}}}\\\hline
\multirow{4}{*}{\bf Hypotheses} & $H_0$ & Proportion of games won at home equals proportion of games won on the road ($p_1=p_2$)\\\cline{2-3}
& $H_a$ & Proportion of games won at home is greater than proportion of games won on the road ($p_1>p_2$)\\\hline
\multirow{4}{*}{\bf Results} & {\sc mlb} & Significant ($p<0.001$) \\\cline{2-3}
& {\sc nhl} & Significant ($p=0.030$) \\\cline{2-3}
& {\sc nba} & Significant ($p<0.001$) \\\cline{2-3}
& Pooled & Significant ($p<0.001$) \\\hline
\end{tabular}\\

\noindent In Part 2, we sought to focus in on the effect of a home crowd on the performance of an individual. We used statistics indicative of individual performance, as opposed to a team's overall record, to test our hypothesis in each of the three leagues considered in Part 1.

Parts 2{\sc a} through 2{\sc c} consider shootouts in the National Hockey League. First, we looked at a team's overall record in the shootout at home and on the road, and conducted a one-tail two-proportion $z$-test at the $5\%$ level.\\

\noindent\begin{tabular}{crp{12cm}}
\multicolumn{3}{l}{\textsf{\textbf{Part 2A: Shootout Win Proportions}}}\\
\multicolumn{3}{l}{\small \textsf{Raw data on page \pageref{raw_part2a}; calculations on page \pageref{part2a}}}\\\hline
\multirow{4}{*}{\bf Hypotheses} & $H_0$ & Proportion of shootout wins at home equals proportion of shootout wins on the road ($p_1=p_2$)\\\cline{2-3}
& $H_a$ & Proportion of shootout wins at home is greater than proportion of shootout wins on the road ($p_1>p_2$)\\\hline
\bf Results & \multicolumn{2}{l}{Not significant ($p=0.989$)}\\\hline
\end{tabular}\\

\noindent We then examined shootouts in even greater detail in Part 2{\sc b}, where we considered shootout goals and saves. In theory, the home crowd should give home-team players a boost and therefore lead to better scoring and save percentages at home in the shootout than on the road. We tested home versus road statistics for goals and saves with a one-tail two-proportion $z$ procedure at the $5\%$ significance level.\\

\noindent\begin{tabular}{crp{12cm}}
\multicolumn{3}{l}{\textsf{\textbf{Part 2B: Shootout Goals \& Saves Proportions}}}\\
\multicolumn{3}{l}{\small \textsf{Raw data on page \pageref{raw_part2b}; calculations on page \pageref{part2b}}}\\\hline
\multirow{4}{*}{\bf Hypotheses} & $H_0$ & Proportion of shootout goals (or saves) at home equals proportion of shootout goals (or saves) on the road ($p_1=p_2$)\\\cline{2-3}
& $H_a$ & Proportion of shootout goals (or saves) at home is greater than proportion of shootout goals (or saves) on the road ($p_1>p_2$)\\\hline
\multirow{2}{*}{\bf Results} & Goals & Not significant ($p=0.758$)\\\cline{2-3}
& Saves & Not significant ($p=0.692$)\\\hline
\end{tabular}\\

\noindent After considering the results from Part 2{\sc b}, we considered that there was a real possibility of individual hockey players actually performing better on the road in shootouts, where there is less pressure from the home crowd. We thus conducted a third test with the same general premises as Part 2{\sc b}, except with the opposite alternative hypothesis.\\

\noindent\begin{tabular}{crp{12cm}}
\multicolumn{3}{l}{\textsf{\textbf{Part 2C: Shootout Goals \& Saves Proportions}}}\\
\multicolumn{3}{l}{\small \textsf{Raw data on page \pageref{raw_part2c}; calculations on page \pageref{part2c}}}\\\hline
\multirow{4}{*}{\bf Hypotheses} & $H_0$ & Proportion of shootout goals (or saves) at home equals proportion of shootout goals (or saves) on the road ($p_1=p_2$)\\\cline{2-3}
& $H_a$ & Proportion of shootout goals (or saves) at home is less than proportion of shootout goals (or saves) on the road ($p_1<p_2$)\\\hline
\multirow{2}{*}{\bf Results} & Goals & Not significant ($p=0.323$)\\\cline{2-3}
& Saves & Significant ($p=0.030$)\\\hline
\end{tabular}\\

\noindent We then considered individual performance results in other sports. Specifically, in Part 2{\sc d}, we looked at free-throw percentages in the {\sc nba}, testing the proportion of free throws made at home versus on the road at the $5\%$ level using a one-tail two-proportion $z$ procedure.\\

\noindent\begin{tabular}{crp{12cm}}
\multicolumn{3}{l}{\textsf{\textbf{Part 2D: Proportion of Free Throws Made}}}\\
\multicolumn{3}{l}{\small \textsf{Raw data on page \pageref{raw_part2d}; calculations on page \pageref{part2d}}}\\\hline
\multirow{4}{*}{\bf Hypotheses} & $H_0$ & Proportion of free throws made at home equals proportion of free throws made on the road ($p_1=p_2$)\\\cline{2-3}
& $H_a$ & Proportion of free throws made at home is greater than proportion of free throws made on the road ($p_1>p_2$)\\\hline
\bf Results & \multicolumn{2}{l}{Not significant ($p=0.567$)}\\\hline
\end{tabular}\\

\noindent In Part 2{\sc e}, we applied to same analysis to baseball, testing whether the average difference between the run differential (runs scored by selected team less runs scored by opposing team) at home and on the road is zero, or is greater than zero (indicating that teams do better at home). The data was tested at the $5\%$ level using a matched-pairs $t$ procedure.\\

\noindent\begin{tabular}{crp{12cm}}
\multicolumn{3}{l}{\textsf{\textbf{Part 2E: Run Differentials}}}\\
\multicolumn{3}{l}{\small \textsf{Raw data on page \pageref{raw_part2e}; calculations on page \pageref{part2e}}}\\\hline
\multirow{4}{*}{\bf Hypotheses} & $H_0$ & Average difference between run differential at home and run differential on the road is zero ($\mu_1-\mu_2=0$)\\\cline{2-3}
& $H_a$ & Average difference between run differential at home and run differential on the road is greater than zero ($\mu_1-\mu_2>0$)\\\hline
\bf Results & \multicolumn{2}{l}{Significant ($p=0.011$)}\\\hline
\end{tabular}\\

\noindent Finally, in Part 2{\sc f}, we looked at baseball again but considering on-base percentages instead, testing whether the players get on base more at home than on the road. The data was tested using a one-tail two-proportion $z$ procedure.\\

\noindent\begin{tabular}{crp{12cm}}
\multicolumn{3}{l}{\textsf{\textbf{Part 2F: On-Base Percentage}}}\\
\multicolumn{3}{l}{\small \textsf{Raw data on page \pageref{raw_part2f}; calculations on page \pageref{part2f}}}\\\hline
\multirow{4}{*}{\bf Hypotheses} & $H_0$ & Proportion of times reached base at home equals proportion of times reached base on the road ($p_1-p_2=0$)\\\cline{2-3}
& $H_a$ & Proportion of times run on base at home is greater than proportion of times run on base on the road ($p_1-p_2>0$)\\\hline
\bf Results & \multicolumn{2}{l}{Significant ($p=0.044$)}\\\hline
\end{tabular}\\

\noindent In Part 3, we shifted our focus to factors affecting the home-team advantage, hypothesizing that the more fans in attendance, the better the home team would do. If this were the case, we would expect a strong correlation between home wins and average home game attendance. We tested this hypothesis for hockey by determining the correlation on data collected from the 2012 season.\\

\noindent\begin{tabular}{ccp{10cm}}
\multicolumn{3}{l}{\textsf{\textbf{Part 3: Home Attendance and Home Wins}}}\\
\multicolumn{3}{l}{\small \textsf{Raw data on pages \pageref{raw_part3a}–\pageref{raw_part3c}; graphs on pages \pageref{part3a_corr}– \pageref{part3c_resid}}}\\\hline
\multirow{2}{*}{\bf Variables} & Independent ($x$) & Average home game attendance\\\cline{2-3}
& Dependent ($y$) & Home game wins\\\hline
\multirow{3}{*}{\bf Results} & \sc mlb & $r=0.269 \rightarrow r^2=0.072$, no pattern evident in residual\\\cline{2-3}
& \sc nba & $r=0.518 \rightarrow r^2=0.268$, no pattern evident in residual\\\cline{2-3}
& \sc nhl & $r=0.310 \rightarrow r^2=0.096$, residual not random (fan pattern)\\\hline
\end{tabular}