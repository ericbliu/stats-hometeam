import rpy2.robjects as robjects
from decimal import *

def parse(s):
	loc = s.index('/')
	n = int(s[:loc])
	d = int(s[loc + 1:])

	return [n, d]

def z_test(count, total, alt, c_l = 0.95, c = False):
	prop_test = robjects.r['prop.test']

	results = prop_test(count, total, alternative = alt, conf_level = c_l, correct = c)

	p_value = str(results.rx2('p.value'))
	p_value = p_value[p_value.index(']') + 1:].strip('\n ')
	samp_prop = results.rx2('estimate')

	sig = "N"

	if Decimal(p_value) <= 0.05:
		sig = "Y"

	p1, p2 = str(count[0]) + "/" + str(total[0]), str(count[1]) + "/" + str(total[1])
	getcontext().prec = 3
	d1, d2 = Decimal(count[0]) / Decimal(total[0]), Decimal(count[1]) / Decimal(total[1])

	combined = sig + " p-value: " + str(p_value) + " p1: " + p1 + " (" + str(d1) + ") p2: " + p2 + " (" + str(d2) + ")"

	return combined

def t_test(list1, list2, alt, m = 0, p = False, ve = False, c_l = 0.95):
	t_test = robjects.r['t.test']

	results = t_test(list1, list2, alternative = alt, mu = m, paired = p, var_equal = ve, conf_level = c_l)

	p_value = str(results.rx2('p.value'))
	p_value = p_value[p_value.index(']') + 1:].strip('\n ')

	sig = "N"

	if Decimal(p_value) <= 0.05:
		sig = "Y"

	l1, l2 = sum(list1), sum(list2)
	getcontext().prec = 3
	diff = Decimal(l1 - l2) / Decimal(len(list1))

	combined = sig + " p-value: " + str(p_value) + " diff: " + str(diff) + " sum l1: " + str(l1) + " sum l2: " + str(l2)

	return combined