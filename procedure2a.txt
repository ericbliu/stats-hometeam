1 Anaheim Ducks
2 Boston Bruins
3 Buffalo Sabres
4 Calgary Flames
5 Carolina Hurricanes
6 Chicago Blackhawks
7 Colorado Avalanche
8 Columbus Blue Jackets
9 Dallas Stars
10 Detroit Red Wings
11 Edmonton Oilers
12 Florida Panthers
13 Los Angeles Kings
14 Minnesota Wild
15 Montreal Canadiens
16 Nashville Predators
17 New Jersey Devils
18 New York Islanders
19 New York Rangers
20 Philadelphia Flyers
21 Phoenix Coyotes
22 Pittsburgh Penguins
23 Ottawa Senators
24 San Jose Sharks
25 St. Louis Blues
26 Tampa Bay Lightning
27 Toronto Maple Leafs
28 Vancouver Canucks
29 Washington Capitals
30 Winnipeg Jets/Atlanta Thrashers

TEAM/HOME SHOOTOUTS/ROAD SHOOTOUTS

2011-2012 (29 15 12)
WASHINGTON 2/4 2/4
MONTREAL 3/11 2/6
FLORIDA 3/12 3/5

2010-2011 (10 8 18)
DETROIT 0/2 4/6
COLUMBUS 5/9 0/4
NY ISLANDERS 1/6 3/4

2009-2010 (5 4 23)
CAROLINA 4/6 0/3
CALGARY 0/3 3/4
OTTAWA 3/6 2/4

2008-2009 (11 2 16)
EDMONTON 1/4 5/6
BOSTON 0/2 4/8
NASHVILLE 4/6 2/5

2007-2008 (8 12 22)
COLUMBUS 2/6 1/5
FLORIDA 2/8 3/3
PITTSBURGH 2/5 5/6