# Execute in R console using source("part3c.r")

library(ggplot2)

data = read.csv(file = "raw3c.csv", header = F)
attendance = data$V1
wins = data$V2
teams = data$V3
print(cor(attendance, wins))

model = lm(wins~attendance)
m = coef(model)
i = signif(m[1], 3)

p = qplot(attendance, wins, 
	main = "Wins and Attendance (NHL 2011-2012 Season)",
	color = attendance, label = teams)
p = p + geom_point() + geom_text(size = 3.75, hjust = 0.25, vjust = 0, family = "Avenir Next Condensed")
p = p + geom_abline(intercept = i, slope = m, color = "#56b4e9") # Predicts wins as a function of attendance
p = p + theme(plot.title = element_text(size = 14, family = "Avenir Next Demi Bold")) 
p = p + theme(axis.title.x = element_text(size = 10, family = "Avenir Next Demi Bold"), 
	axis.text.x  = element_text(size = 9, family = "Avenir Next")) + scale_x_continuous(name = "Average Home Attendance")
p = p + theme(axis.title.y = element_text(size = 10, family = "Avenir Next Demi Bold"), 
	axis.text.y  = element_text(size = 9, family = "Avenir Next")) + scale_y_continuous(name = "Home Wins")
p = p + theme(legend.title = element_text(size = 10, family = "Avenir Next Demi Bold"), 
	legend.text = element_text(size = 9, family = "Avenir Next")) + scale_colour_gradient2(name = "Attendance")
print(p)
dev.new()

q = qplot(attendance, resid(model), 
	main = "Residual, Wins and Attendance", 
	color = attendance) + geom_hline(aes(yintercept=0), color = "#56b4e9")
q = q + scale_colour_gradient2(guide = F)
q = q + theme(plot.title = element_text(size = 14, family = "Avenir Next Demi Bold")) 
q = q + theme(axis.title.x = element_text(size = 10, family = "Avenir Next Demi Bold"), 
 	axis.text.x  = element_text(size = 9, family = "Avenir Next")) + scale_x_continuous(name = "Average Home Attendance")
q = q + theme(axis.title.y = element_text(size = 10, family = "Avenir Next Demi Bold"), 
 	axis.text.y  = element_text(size = 9, family = "Avenir Next")) + scale_y_continuous(name = "Residual (Home Wins)")
print(q)