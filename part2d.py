import rpy2.robjects as robjects
from decimal import *
from tools import *

class Year:
    def __init__(self, y, r):
        self.year = y

        home, road = [0, 0], [0, 0]

        for i in range(0, len(r), 2):
            home = [a + b for a, b in zip(home, parse(r[i]))]
            road = [a + b for a, b in zip(road, parse(r[i + 1]))]

        self.records = [home, road]

        self.results = ""

    def get_proportions(self):
        return self.year + " " + str(self.results)

    def __repr__(self):
        return self.name + "\n" + str(self.records)

years = []

for line in open("raw2d.csv", 'r'):
    tmp = line.rstrip('\n\r ').split(',')

    years.append(Year(tmp[0], tmp[1:]))

for y in years:
    r = y.records

    y.results = z_test(robjects.IntVector((r[0][0], r[1][0])), robjects.IntVector((r[0][1], r[1][1])), "greater")

for y in years:
        print(y.get_proportions())

# Pooling everything together

hw, ht, aw, at = 0, 0, 0, 0

for y in years:
    r = y.records

    hw += r[0][0]
    ht += r[0][1]
    aw += r[1][0]
    at += r[1][1]

print("Pooled " + z_test(robjects.IntVector((hw, aw)), robjects.IntVector((ht, at)), "greater"))